var express = require("express");
var cons = require("consolidate");
var http = require("http");
var path = require("path");
var fs = require("fs");
var swig = require('swig');

// Database config
var mongoose = require("mongoose");
mongoose.connect("mongodb://localhost/bitter")
var db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function callback() {
	console.log("Successful connection to db");
});

// Get models
var modelDir = path.join(__dirname, 'models');
fs.readdirSync(modelDir).forEach( function(file) {
	if (~file.indexOf('.js')) {
		require("./models/" + file);
	}
});

// Routes
var routes = require("./routes");
var main = require("./routes/main");
var tags = require("./routes/tags")

var app = express();

// Configuration
app.set("port", process.env.PORT || 3000);
app.engine("html", cons.swig);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');

// yes that's our secret
app.use(express.cookieParser("secret_key")); 

// Static content
app.use(express.static(path.join(__dirname, '/static')));

// Middleware
app.use(express.json());
app.use(express.bodyParser());

// Request handlers
app.get('/', routes.index);
app.post('/', routes.index_post);
app.get(new RegExp('^\/user\/[a-zA-Z0-9]{3,20}$'), main.mainReq);
app.post(new RegExp('^\/user\/[a-zA-Z0-9]{3,20}$'), main.main_post);
app.get(new RegExp('^\/@\/[a-zA-Z0-9]+$'), tags.tagPage);
app.get('/logout', main.logOut);

require("./tests/maketest")(app);

// Create server
http.createServer(app).listen(app.get('port'), function() {
 	console.log('Express server listening on port ' + app.get('port'));
});