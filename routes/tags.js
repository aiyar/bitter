var mongoose = require("mongoose");
var User = mongoose.model("User");
var Post = mongoose.model("Post");

exports.tagPage = function(req, res) {
	var tag = "#" + req.url.substring(3,req.url.length);
	Post.find({tags : tag}).populate("_creator").exec(function(err, posts) {
		if (err || !posts) {
			res.send("Problems with requested url");
		}
		else {
			// we're going to display unused tags anyway
			renderTagPage(tag,posts,res);
		}
	});
}

var renderTagPage= function(tag, posts, res) {
	var tweets = [];
	locals = {};
	for (var i = 0; i < posts.length; ++i) {
		tweets.push({
			username: posts[i]._creator.username,
			date: posts[i].date.toDateString(),
			details: renderPost(posts[i].details)
		});
	}

	locals["tagName"] = tag;
	locals["tweets"] = tweets;
	res.render("tags", locals);
}

var renderPost = function(str) {
	return str.replace(/(\#)(\w+)/g, replaceHash);
}

var replaceHash = function(str, hashsym, tag) {
	return "<a class=\'hashtag\' href=\'../@/" + tag + "\'>" + str + "</a>";
}