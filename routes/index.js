/* Router handlers for main page*/

var mongoose = require("mongoose");
var User = mongoose.model("User");

// get '/'
exports.index = function(req, res) {
	res.sendfile("static/login.html");
};

// post '/'
exports.index_post = function(req, res) {
	
	// Get details of post from body
	var body = req.body;
	var page = body["post-submit"];

	if (page == "Login") {
		console.log("Logging In");
		validateLogin(res, body);
	}
	else {
		console.log("Signing Up");
		processSignUp(res, body);
	}

};

var makeTest = function() {
	console.log(User.findOne({"username" : "john"}));
	var john = new User();
	john.username = "john";
	john.salt = john.genSalt();
	john.hashed_pass = john.encrypt("monkey");
	john.email = "john@john.com";
	john.save();
}

var validateLogin = function(res, postReq) {

	// TODO: This is riddiculously ugly: refactor

	var hasError = false;
	var errorList = []; // Let me tell the user why he's stupid!
	var uInfo = {}; // User info object
	var eInfo = {}; // Error info object to pass to Swig

	uInfo["username"] = postReq["login-name"];

	User.findOne(uInfo, function(err, user) {
		if (err) {
			// No clue what kind of error is going on here
			console.log("Error finding user with User.findOne()");
			hasError = true;
		}
		else if (!user) {
			// User doesn't exist, good work Sherlock
			console.log("Didn't find the user:")
			hasError = true;
			eInfo["login_name_error"] = true;
			errorList.push("Invalid username");
		}
		else {
			// User exists, time to validate
			if(!user.auth(postReq["login-pass"])) {
				hasError = true;
				eInfo["login_pass_error"] = true;
				errorList.push("Invalid Password")
			}
		}

		// How to render
		if (hasError) {
			eInfo["errorList"] = errorList;
			res.render("login", eInfo);
		}
		else {
			res.cookie("remember", uInfo["username"], {expires: new Date(Date.now() + 900000)});
			res.redirect("/user/" + uInfo["username"]);
		}

	});
}

var processSignUp = function(res, postReq) {
	var hasError = false;
	var errorList = [];
	var eInfo = {};
	var uInfo = {};
	
	uInfo["username"] = postReq["user-name"];

	User.findOne(uInfo, function(err, user) {
		if (err) {
			console.log("Houston we have a problem with User.finOne");
		}
		else if (!user) {
			// validate and create
 			if(!validName(postReq["user-name"])) {
 				hasError = true;
 				eInfo["user_name_error"] = true;
 				errorList.push("Invalid username. 3-20 Alphanumeric");
 			}
 			if(!validPass(postReq["user-pass"])) {
 				hasError = true;
 				eInfo["user_pass_error"] = true;
 				errorList.push("Invalid password. 4-32 characters including !@#$%^&*");
 			}
 			if(postReq["user-pass"] != postReq["user-pass2"]) {
 				hasError = true;
 				eInfo["user_pass2_error"] = true;
 				errorList.push("Passwords do not match");
 			}
 			if(!validEmail(postReq["user-email"])) {
 				hasError = true;
 				eInfo["user_email_error"] = true;
 				errorList.push("Invalid email address.");
 			}
		}
		else {
			// user exists, choose a different name
			errorList.push("Username already in use.");
			hasError = true;
		}


		if(hasError) {
			eInfo["errorList"] = errorList;
			eInfo["signupError"] = true;
			eInfo["signup_dominant"] = "signup_dominant";
			res.render("login", eInfo);
		}
		else {
			createUser(postReq);
			var locals = {};
			locals["success"] = true;
			locals["username"] = postReq["user-name"];
			res.render("login", locals);
		}
	});

}

var createUser = function(postReq) {
	// Called after validation
	var user = new User();
	user.username = postReq["user-name"];
	user.salt = user.genSalt();
	user.hashed_pass = user.encrypt(postReq["user-pass"]);
	user.email = postReq["user-email"];
	user.save(); 
}

var validName = function(username) {
	// Between 3 and 20 chars from [A-Za-z0-9_]
	return /^\w{3,20}$/.test(username);
}

var validEmail = function(email) {
	var re = /[a-z0-9!#$%&'*+/=?^_{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
	return re.test(email);
}

var validPass = function(pass) {
	// Between 4 and 32 chars with special symbols
	return /^(\w|[!@#$%^&*]){4,32}$/.test(pass);
}