var mongoose = require("mongoose");
var User = mongoose.model("User");
var Post = mongoose.model("Post");

// get 'user/username'  handler
exports.mainReq = function(req, res) {
	var name = req.url.substring(6,req.url.length);
	var locals = {};
	locals["username"] = name;

	if (req.cookies.remember == name) {
		locals["showInput"] = true;
		locals["loggedIn"] = true;
		locals["loggedInAs"] = "Logged in as " + name;
	}

	User.findOne({username : name}, function(err, user) {
		if(err) console.log("error finding" + name);
		else if(!user) {
			res.send(name + " does not exist");
		}
		else {
			Post.find({_creator: user._id})
				.sort({date: -1})
				.exec(function(err, posts) {
					if(err) console.log("error finding posts");
					else if (!posts) {
						console.log("No posts exists for " + user.username);
						res.render('main', locals);
					}
					else {
						renderUserPage(posts,res,locals);
					}
				});
		}
	});
}

// post 'user/username' handler
exports.main_post = function(req, res) {
	// validate the poster (clearly not the most secure way, can still spoof cookie)
	var name = req.url.substring(6,req.url.length);
	if (req.cookies.remember != name) {
		console.log("Fake user posting data at " + req.url);
		res.redirect(req.url);
	}
	else {
		var tweet = req.body["tweet-input"];

		// validate tweet length
		if ( tweet.length < 1 || tweet.length > 140) {
			res.redirect(req.url);
		}
		else {
			User.findOne({username: name}, function(err, user) {
				if (user) {
					var post = new Post();
					post.details = escapeHTML(tweet);
					post.date = Date.now();
					post.gatherHashtags();
					post._creator = user._id;
					post.save();
				}
				res.redirect(req.url);
			});
		}
	}
}

// get '/logout' handler 
exports.logOut = function(req, res) {
	res.clearCookie('remember');
	res.redirect('/');
}

var renderUserPage = function(posts, res, locals) {
	var tweets = [];
	for (var i = 0; i < posts.length; ++i) {
		tweets.push({
			username: locals.username,
			date: posts[i].date.toDateString(),
			details: renderPost(posts[i].details)
		});
	}
	locals["tweets"] = tweets;
	res.render("main", locals);
}

var renderPost = function(str) {
	return str.replace(/(\#)(\w+)/g, replaceHash);
}

var replaceHash = function(str, hashsym, tag) {
	return "<a class=\'hashtag\' href=\'../@/" + tag + "\'>" + str + "</a>";
}

// Ghetto post sanitizer from: 
// http://shebang.brandonmintern.com/foolproof-html-escaping-in-javascript/
var MAP = { '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#39;'};

function escapeHTML (s, forAttribute) {
    return s.replace(forAttribute ? /[&<>'"]/g : /[&<>]/g, function(c) {
        return MAP[c];
    });
}