// Reference: http://mongoosejs.com/docs/guide.html
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var crypto = require("crypto");

// Schema for user data
var UserSchema = new Schema({
	
	username: {
		type: String,
		default: ""
	},

	hashed_pass: {
		type: String,
		default: ""
	},

	salt: {
		type: String,
		default: ""
	},

	email: {
		type: String,
		default: ""
	},
	
});

// Random 5 digit number
UserSchema.methods.genSalt = function() {
	return Math.floor(Math.random() * 90000) + 10000;
}

// Plaintext === hash?
UserSchema.methods.auth = function(plain_pass) {
	return this.encrypt(plain_pass) === this.hashed_pass;
}

// Salt and sha256 it
UserSchema.methods.encrypt = function(plain_pass) {
	var shasum = crypto.createHash("sha256");
	shasum.update(plain_pass + this.salt);
	return shasum.digest("hex");
}

// http://mongoosejs.com/docs/api.html#connection_Connection-model
// The collection name will be "users" in mongo, unless we specify
// otherwise in the 3rd parameter
mongoose.model("User", UserSchema);