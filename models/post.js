var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var Post = new Schema({

	_creator: {
		type: Schema.ObjectId, 
		ref: "User"
	},

	details: {
		type: String,
		default: ""
	},

	tags: [String],

	date: { 
		type: Date,
		default: Date.now
	}

});

Post.methods.gatherHashtags = function() {
	var tags = getTags(this.details);
	for (var i = 0; i < tags.length; ++i) {
		this.tags.push(tags[i]);
	}
}

var getTags = function(str) {
	var tags = str.match(/(\#)(\w+)/g);
	if(!tags) tags = [];
	return tags;
}

mongoose.model('Post', Post);