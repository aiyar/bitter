// Create Test Users and Posts
var mongoose = require("mongoose");
var User = mongoose.model("User");
var Post = mongoose.model("Post");

module.exports = function(app) {
	app.get("/tests/user", function(req, res) {
		createUsers();
		res.send("Test users created");
	}); 
	app.get("/tests/post", function(req, res) {
		createPosts();
		res.send("Test posts created");
	}); 
}


var createUsers = function() {
	
	var names = ["harry", "ron", "hermione"];

	for (var i = 0; i < 3; ++i) {
		var user = new User();
		user.username = names[i];
		user.salt = user.genSalt();
		user.hashed_pass = user.encrypt("password");
		user.email = "user@user.com";
		user.save();
	}

}

var createPosts = function() {

	var data = [];
	data.push("They're called Thestrals. People avoid them because they're a bit different #akward");
	data.push("Oh look it's looney lovegood #akward");
	data.push("It's funny that I can do #magic but I'm broke, sup with that #logic");
	data.push("Man, professor long beard is sure them there goods with the #logic");
	data.push("Ok, totally meant #magic. Woops. Good thing we don't have edits! #lazy");
	data.push("You're a god damn wizard harry #magic");
	data.push("It's wingardrium leviosa...not wingardrium leviosa #magic");

	var harry = User.findOne({username: "harry"}, function(err, user) {
		if(user) {
			for (var i = 0; i < 2; ++i) {
				var post = new Post();
				post.details = data[i];
				post.gatherHashtags();
				post.date = Date.now() + (i+1)*3600;
				post._creator = user._id;
				post.save();
			}
		}
	});


	var ron = User.findOne({username: "ron"}, function(err, user) {
		if(user) {
			for (var i = 2; i < 5; ++i) {
				var post = new Post();
				post.details = data[i];
				post.date = Date.now() + (i+1)*3600;
				post.gatherHashtags();
				post._creator = user._id;
				post.save();
			}
		}
	});

	var herm = User.findOne({username: "hermione"}, function(err, user) {
		if(user) {
			for (var i = 5; i < 7; ++i) {
				var post = new Post();
				post.details = data[i];
				post.date = Date.now() + (i+1)*3600;
				post.gatherHashtags();
				post._creator = user._id;
				post.save();
			}
		}
	});

}