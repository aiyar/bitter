$(document).ready(function() {

	var isToggled = false;

	$(".okbtn").click(function() {
		if ($("input[name=tweet-input]").val().length > 140) {
			console.log("input is too damn long");
			return false;
		}
		return true;
	});

	$("input[name=tweet-input]").keyup(function() {
		var size = $(this).val().length;

		if ((140-size) <= 20) {
			if (!isToggled) {
				isToggled = true;
				toggleButton();
			}
			$(".okbtn").text(140 - size);
		}
		else {
			if (isToggled) {
				isToggled = false;
				toggleButton();
			}
		}
	});
});

var toggleButton = function() {
	$(".okbtn").toggleClass("btn-success btn-warning")
	$(".okbtn").text("Ok");
}