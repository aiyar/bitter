$(document).ready(function() {
	animateLogin();
	startSignUp();
	startLogin();
	validateSignUp();
});

var globals = {
	loginVisible  : true
}

var removeDialogs = function() {
	$(".success-message").remove();
	$(".signup_dominant").remove();
	$(".error-dialogue").remove();
}

var animateLogin = function() {
	if ($(".signup_dominant").length != 0) {
		globals.loginVisible = false;
		$(".signup-container").show();
		$(".login-content").show();
	}
	else {
		var lcont = $(".login-container");
		var lstuff = $(".login-content");
		lcont.slideDown(600, function() {
			lstuff.fadeIn(1000);
		});
	}
}

var startSignUp = function() {
	var signUp = $(".signup-link");
	signUp.click(function() {
		toggleLoginSwitch();
	});
}

var startLogin = function() {
	var goback = $(".goback-link");
	goback.click(function() {
		toggleLoginSwitch();
	});
}

// Swap between login and signup
var toggleLoginSwitch = function() {
	removeDialogs();
	var lcont = $(".login-container");
	var scont = $(".signup-container");
	if (globals.loginVisible) {
		globals.loginVisible = false;
		lcont.hide("slide", {direction: "left"}, 500, function() {
			scont.show("slide", {direction:"right"}, 500);
		});
	}
	else {
		globals.loginVisible = true;
		scont.hide("slide", {direction: "left"}, 500, function() {
			lcont.show("slide", {direction:"right"}, 500);
		});
	}
}

var validateSignUp = function() {
	$("#signup-form").on("submit", function() {
		if($("#user-pass").val() != $("#user-pass2").val()) {
			$(".user-pass").addClass("has-error");
			$(".user-pass2").addClass("has-error");
			return false;
		}
		else {
			$(".user-pass").removeClass("has-error");
			$(".user-pass2").removeClass("has-error");
		}
		if(!validateEmail($("#user-email").val())) {
			$(".user-email").addClass("has-error");
			return false;
		}
		else {
			$(".user-email").removeClass("has-error");
		}
		return true;
	});
}

function validateEmail(email) {
	var re = /[a-z0-9!#$%&'*+/=?^_{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    return re.test(email);
}